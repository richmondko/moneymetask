//
//  ErrorUtil.swift
//  MoneyMeTask
//
//  Created by Richmond Ko on 08/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import Foundation

struct ErrorUtil {
    enum ErrorMessage: String {
        case selectLoanAmountandDuration = "Please select a loan amount and loan duration above."
        case missingLoanReason = "Please input a loan reason."
        case missingFirstName = "Please input your first name."
        case missingLastName = "Please input your last name."
        case missingBirthday = "Please select your birthday."
        case invalidEmail = "Please input a valid email."
        case invalidMobileNumber = "Please input a valid mobile number."
        case invalidPassword = "Please input a password."
        case genericError = "Oops, something went wrong. Try again later."
    }
}
