//
//  Constants.swift
//  MoneyMeTask
//
//  Created by Richmond Ko on 08/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import Foundation

enum Constant: Int {
    case minimumLoanAmount = 2100
    case maximumLoanAmount = 15000
    case minimumLoanDuration = 3
    case maximumLoanDuration = 36
    case loanIncrements = 100
}

