//
//  DesignableButton.swift
//  MoneyMeTask
//
//  Created by Richmond Ko on 08/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import Foundation
import UIKit

class DesignableButton: UIButton {
    @IBInspectable var cornerRadius: Double {
        get {
            return Double(self.layer.cornerRadius)
        }set {
            self.layer.cornerRadius = CGFloat(newValue)
        }
    }
}
