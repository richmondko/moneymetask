//
//  MMPopupViewController.swift
//  MoneyMeTask
//
//  Created by Richmond Ko on 08/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import Foundation
import UIKit

fileprivate enum ModalTransitionType {
    case presentation, dismissal
}

class BottomSheetViewController: UIViewController {

    // MARK: Stored Properties
    var bottomSheetContainerView: UIView!
    fileprivate var currentModalTransitionType: ModalTransitionType? = nil
    fileprivate static let overlayBackgroundColor = UIColor.black.withAlphaComponent(0.3)

    // MARK: App View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        configureSwipeDownGesture()
        configureTapGesture()
    }

    // MARK: Instance Functions
    private func configureUI() {
        transitioningDelegate = self
        bottomSheetContainerView.clipsToBounds = true
        bottomSheetContainerView.layer.cornerRadius = 12.0
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }

    private func configureSwipeDownGesture() {
        let swipeDownGesture = UISwipeGestureRecognizer(target: self, action: #selector(didSwipeDown(_:)))
        swipeDownGesture.direction = .down
        view.addGestureRecognizer(swipeDownGesture)
    }

    private func configureTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTap(_:)))
        tapGesture.delegate = self
        view.addGestureRecognizer(tapGesture)
    }

    @objc private func didSwipeDown(_ gesture: UISwipeGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }

    @objc private func didTap(_ gesture: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }
}

extension BottomSheetViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == self.view
    }
}

extension BottomSheetViewController: UIViewControllerTransitioningDelegate {
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let result = (presented == self) ? self : nil
        result?.currentModalTransitionType = .presentation
        return result
    }

    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let result = (dismissed == self) ? self : nil
        result?.currentModalTransitionType = .dismissal
        return result
    }
}

extension BottomSheetViewController: UIViewControllerAnimatedTransitioning {
    private var transitionDuration: TimeInterval {
        guard let transitionType = self.currentModalTransitionType else { fatalError() }
        switch transitionType {
        case .presentation:
            return 0.44
        case .dismissal:
            return 0.32
        }
    }

    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return transitionDuration
    }

    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let transitionType = self.currentModalTransitionType else { fatalError() }

        // Here's the state we'd be in when the card is offscreen
        let cardOffscreenState = {
            let offscreenY = self.view.bounds.height - self.bottomSheetContainerView.frame.minY + 20
            self.bottomSheetContainerView.transform = CGAffineTransform.identity.translatedBy(x: 0, y: offscreenY)
            self.view.backgroundColor = .clear
        }

        // ...and here's the state of things when the card is onscreen.
        let presentedState = {
            self.bottomSheetContainerView.transform = CGAffineTransform.identity
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        }

        // We want different animation timing, based on whether we're presenting or dismissing.
        let animator: UIViewPropertyAnimator
        switch transitionType {
        case .presentation:
            animator = UIViewPropertyAnimator(duration: transitionDuration, dampingRatio: 0.82)
        case .dismissal:
            animator = UIViewPropertyAnimator(duration: transitionDuration, curve: UIView.AnimationCurve.easeIn)
        }

        switch transitionType {
        case .presentation:
            // We need to add the modal to the view hierarchy,
            // and perform the animation.
            let toView = transitionContext.view(forKey: .to)!
            UIView.performWithoutAnimation(cardOffscreenState)
            transitionContext.containerView.addSubview(toView)
            animator.addAnimations(presentedState)
        case .dismissal:
            // The modal is already in the view hierarchy,
            // so we just perform the animation.
            animator.addAnimations(cardOffscreenState)
        }

        // When the animation finishes,
        // we tell the system that the animation has completed,
        // and clear out our transition type.
        animator.addCompletion { (position) in
            assert(position == .end)
            transitionContext.completeTransition(true)
            self.currentModalTransitionType = nil
        }

        // ... and here's where we kick off the animation:
        animator.startAnimation()
    }
}

