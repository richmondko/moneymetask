//
//  Date+formats.swift
//  MoneyMeTask
//
//  Created by Richmond Ko on 08/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import Foundation

extension Date {
    func toString() -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter.string(from: self)
    }
}
