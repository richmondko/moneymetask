//
//  LoanApplicationFormViewModel.swift
//  MoneyMeTask
//
//  Created by Richmond Ko on 08/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import Foundation

protocol LoanApplicationFormViewModelDelegate: class {
    func showValidationError(withMessage message: String)
}

class LoanApplicationFormViewModel {
    private let annualInterestRate = 0.0899
    var loanIncrements: [Int] = []
    var loanDurationIncrements: [Int] = []
    var loggedInUser: MMUser? = nil
    weak var delegate: LoanApplicationFormViewModelDelegate?

    // form data
    var loanAmount: Binding<Int> = Binding(0)
    var loanDuration: Binding<Int> = Binding(0)
    var loanReason: String = ""
    var title: String = ""
    var firstName: String = ""
    var lastName: String = ""
    var selectedBirthday: Binding<Date?> = Binding(nil)
    var emailAddress: String = ""
    var mobileNumber: String = ""
    var password: String = ""

    // presenter
    var loanAmountString: String = ""
    var loanDurationString: String = ""
    var loanPMT: String = ""

    init(loanAmount: Int, loanDuration: Int, user: MMUser? = nil, delegate: LoanApplicationFormViewModelDelegate) {
        self.loanAmount.value = loanAmount
        self.loanDuration.value = loanDuration

        if let user = user {
            loggedInUser = user
            if let dateOfBirthTimestamp = user.dateOfBirth {
                self.selectedBirthday.value = Date(timeIntervalSince1970: dateOfBirthTimestamp)
            }
            self.title = user.title ?? ""
            self.firstName = user.firstName ?? ""
            self.lastName = user.lastName ?? ""
            self.mobileNumber = user.mobileNumber ?? ""
            self.emailAddress = user.email ?? ""
        }

        self.delegate = delegate
    }

    func initialize() {
        configureLoanAmount()
        configureLoanDuration()
        generateLoanIncrements()
        generateMonthIncrements()
        configurePMT()
    }

    func configureLoanAmount() {
        self.loanAmountString = "$\(loanAmount.value.formatWithDecimal)"
    }

    func configureLoanDuration() {
        self.loanDurationString = "\(loanDuration.value.formatWithDecimal) months"
    }

    func configurePMT() {
        let payments = ExcelFormulas.pmt(rate: (annualInterestRate)/12, nper: Double(loanDuration.value), pv: -Double(loanAmount.value))
        loanPMT = "$\(payments.formatWithDecimal) Monthly Repayment"
    }

    private func generateLoanIncrements() {
        var currentIncrement = Constant.minimumLoanAmount.rawValue
        loanIncrements.append(currentIncrement)

        while currentIncrement != Constant.maximumLoanAmount.rawValue {
            currentIncrement += Constant.loanIncrements.rawValue
            loanIncrements.append(currentIncrement)
        }
    }

    private func generateMonthIncrements() {
        var currentIncrement = Constant.minimumLoanDuration.rawValue
        loanDurationIncrements.append(currentIncrement)

        while currentIncrement != Constant.maximumLoanDuration.rawValue {
            currentIncrement += 1
            loanDurationIncrements.append(currentIncrement)
        }
    }

    func isFormValidated() -> Bool {

        if loanReason.isEmpty {
            delegate?.showValidationError(withMessage: ErrorUtil.ErrorMessage.missingLoanReason.rawValue)
            return false
        }

        if firstName.isEmpty {
            delegate?.showValidationError(withMessage: ErrorUtil.ErrorMessage.missingFirstName.rawValue)
            return false
        }

        if lastName.isEmpty {
            delegate?.showValidationError(withMessage: ErrorUtil.ErrorMessage.missingLastName.rawValue)
            return false
        }

        if selectedBirthday.value == nil {
            delegate?.showValidationError(withMessage: ErrorUtil.ErrorMessage.missingBirthday.rawValue)
            return false
        }

        if !isValidEmail(emailStr: emailAddress) {
            delegate?.showValidationError(withMessage: ErrorUtil.ErrorMessage.invalidEmail.rawValue)
            return false
        }

        if mobileNumber.isEmpty {
            delegate?.showValidationError(withMessage: ErrorUtil.ErrorMessage.invalidMobileNumber.rawValue)
            return false
        }

        guard loggedInUser == nil else { return true }
        
        if password.isEmpty {
            delegate?.showValidationError(withMessage: ErrorUtil.ErrorMessage.invalidPassword.rawValue)
            return false
        }

        return true
    }

    private func isValidEmail(emailStr: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
}
