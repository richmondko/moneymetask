//
//  MMUser.swift
//  MoneyMeTask
//
//  Created by Richmond Ko on 08/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import Foundation

struct MMUser: Codable {
    var id: String? = nil
    let firstName: String?
    let lastName: String?
    let title: String?
    let dateOfBirth: Double?
    let email: String?
    let mobileNumber: String?

    enum CodingKeys: String, CodingKey {
        case firstName = "first_name"
        case lastName = "last_name"
        case title = "title"
        case dateOfBirth = "date_of_birth"
        case email = "email"
        case mobileNumber = "mobile_number"    }
}

struct MMLoan: Codable {
    let userId: Int?
    let loanAmount: Int?
    let loanDuration: Int?
    let loanReason: String?

    enum CodingKeys: String, CodingKey {
        case userId = "user_id"
        case loanAmount = "loan_amount"
        case loanDuration = "loan_duration"
        case loanReason = "loan_reason"
    }
}
