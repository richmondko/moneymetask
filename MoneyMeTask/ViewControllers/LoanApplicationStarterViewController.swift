//
//  ViewController.swift
//  MoneyMeTask
//
//  Created by Richmond Ko on 07/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import UIKit

class LoanApplicationStarterViewController: UIViewController {

    // MARK: - Stored (IBOutlet)
    @IBOutlet var loanAmountSlider: UISlider!
    @IBOutlet var loanDurationSlider: UISlider!
    @IBOutlet var loanAmountToolTipView: DesignableView!
    @IBOutlet var loanDurationToolTipView: DesignableView!
    @IBOutlet var loanAmountLabel: UILabel!
    @IBOutlet var loanDurationLabel: UILabel!
    @IBOutlet var loanAmountLabelCenterConstraint: NSLayoutConstraint!
    @IBOutlet var loanDurationLabelCenterConstraint: NSLayoutConstraint!

    // MARK: - App View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSliders()
    }

    // MARK: - Instance
    private func configureSliders() {
        let minimumLoanAmount = Float(Constant.minimumLoanAmount.rawValue)
        let maximumLoanAmount = Float(Constant.maximumLoanAmount.rawValue)

        loanAmountSlider.minimumValue = minimumLoanAmount
        loanAmountSlider.maximumValue = maximumLoanAmount
        loanAmountSlider.value = ((maximumLoanAmount - minimumLoanAmount) / 2) + minimumLoanAmount

        let minimumLoanDuration = Float(Constant.minimumLoanDuration.rawValue)
        let maximumLoanDuration = Float(Constant.maximumLoanDuration.rawValue)

        loanDurationSlider.minimumValue = minimumLoanDuration
        loanDurationSlider.maximumValue = maximumLoanDuration
        loanDurationSlider.value = ((maximumLoanDuration - minimumLoanDuration) / 2) + minimumLoanDuration
    }

    private func repositionToolTipView(for slider: UISlider, toolTipCenterContraint: NSLayoutConstraint) {
        let tractRect = slider.trackRect(forBounds: slider.bounds)
        let thumbRect = slider.thumbRect(forBounds: slider.bounds, trackRect: tractRect, value: slider.value)
        let sliderHalfWidth = tractRect.width / 2
        let thumHalfWidth = thumbRect.width / 2
        toolTipCenterContraint.constant = thumbRect.origin.x - sliderHalfWidth + thumHalfWidth
        view.layoutIfNeeded()
    }

    private func showLoanApplicationForm(user: MMUser? = nil) {
        let loanApplicationForm = LoanApplicationFormViewController.instantiateFromStoryboard()
        loanApplicationForm.loanApplicationFormViewModel = LoanApplicationFormViewModel(loanAmount: Int(loanAmountSlider.value), loanDuration: Int(loanDurationSlider.value), user: user, delegate: loanApplicationForm)
        present(loanApplicationForm, animated: true, completion: nil)
    }

    private func isThereSelectedLoanAmountAndDuration() -> Bool {
        return loanAmountLabel.text != "-Select-" && loanDurationLabel.text != "-Select-"
    }

    private func showAuthenticationPopup() {
        let authenticationPopupViewController = AuthenticationPopupViewController.instantiateFromStoryboard()
        authenticationPopupViewController.delegate = self
        present(authenticationPopupViewController, animated: true, completion: nil)
    }

    private func showLogin() {
        let loginViewController = LoginViewController.instantiateFromStoryboard()
        loginViewController.delegate = self
        present(loginViewController, animated: true, completion: nil)
    }

    // MARK: - Instance (IBAction)
    @IBAction func didSlideAmountSlider(_ sender: UISlider) {
        repositionToolTipView(for: sender, toolTipCenterContraint: loanAmountLabelCenterConstraint)
        loanAmountLabel.text = "$\(sender.value.clean)"
    }

    @IBAction func didSlideLoanDurationSlider(_ sender: UISlider) {
        repositionToolTipView(for: sender, toolTipCenterContraint: loanDurationLabelCenterConstraint)
        loanDurationLabel.text = "\(sender.value.clean) months"
    }

    @IBAction func didTapApplyNowButton(_ sender: Any) {
        if isThereSelectedLoanAmountAndDuration() {
            showAuthenticationPopup()
        } else {
            DisplayUtil.displayOkAlert(text: ErrorUtil.ErrorMessage.selectLoanAmountandDuration.rawValue, parentViewController: self)
        }
    }
}

extension LoanApplicationStarterViewController: AuthenticationPopupDelegate {
    func didTapMemberLogin() {
        showLogin()
    }

    func didTapStartApplication() {
        showLoanApplicationForm()
    }
}

extension LoanApplicationStarterViewController: LoginViewControllerDelegate {
    func didFinishLogin(user: MMUser) {
        showLoanApplicationForm(user: user)
    }
}
