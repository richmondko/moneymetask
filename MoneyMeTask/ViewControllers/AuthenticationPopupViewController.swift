//
//  AuthenticationPopupViewController.swift
//  MoneyMeTask
//
//  Created by Richmond Ko on 08/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import UIKit

protocol AuthenticationPopupDelegate: class {
    func didTapStartApplication()
    func didTapMemberLogin()
}

class AuthenticationPopupViewController: BottomSheetViewController {

    weak var delegate: AuthenticationPopupDelegate?

    // MARK: - Stored (IBOutlet)
    @IBOutlet var popupContainerView: UIView!

    // MARK: - App View Life Cycle
    override func viewDidLoad() {
        self.bottomSheetContainerView = popupContainerView
        super.viewDidLoad()
    }

    static func instantiateFromStoryboard() -> AuthenticationPopupViewController {
        return UIStoryboard(name: "AuthenticationPopup", bundle: nil).instantiateViewController(withIdentifier: "AuthenticationPopupViewController") as! AuthenticationPopupViewController
    }

    // MARK: - Instance (IsBAction)
    @IBAction func didTapStartApplication(_ sender: Any) {
        self.dismiss(animated: true) { [weak self] in
            self?.delegate?.didTapStartApplication()
        }
    }

    @IBAction func didTapMemberLogin(_ sender: Any) {
        self.dismiss(animated: true) { [weak self] in
            self?.delegate?.didTapMemberLogin()
        }
    }
}
