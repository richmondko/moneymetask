//
//  LoginViewController.swift
//  MoneyMeTask
//
//  Created by Richmond Ko on 08/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import UIKit
import Firebase

protocol LoginViewControllerDelegate: class {
    func didFinishLogin(user: MMUser)
}

class LoginViewController: UIViewController {

    // MARK: - Stored
    weak var delegate: LoginViewControllerDelegate?

    // MARK: - Stored (IBOutlet)
    @IBOutlet var emailAddressTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var loginButton: DesignableButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - App View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTextFields()
        hideActivityIndicator()
    }

    // MARK: - Instance
    static func instantiateFromStoryboard() -> LoginViewController {
        return UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
    }

    private func configureTextFields() {
        emailAddressTextField.delegate = self
        passwordTextField.delegate = self
    }

    private func showActivityIndicator() {
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
        loginButton.isEnabled = false
    }

    private func hideActivityIndicator() {
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
        loginButton.isEnabled = true
    }

    // MARK: - Instance (IBAction)
    @IBAction func didTapLogin(_ sender: Any) {
        showActivityIndicator()
        FirebaseService.shared.loginUser(email: emailAddressTextField.text!, password: passwordTextField.text!) { [weak self] (user, error) in
            guard let self = self else { return }
            self.hideActivityIndicator()
            if let user = user {
                self.showActivityIndicator()
                FirebaseService.shared.getUserEntry(userId: user.uid, completion: { (mmUser, error) in
                    self.hideActivityIndicator()
                    if let mmUser = mmUser {
                        self.dismiss(animated: true, completion: {
                            self.delegate?.didFinishLogin(user: mmUser)
                        })
                    } else if let error = error {
                        DisplayUtil.displayOkAlert(title: "Error", message: error.localizedDescription, parentViewController: self)
                    } else {
                        DisplayUtil.displayOkAlert(title: "Error", message: ErrorUtil.ErrorMessage.genericError.rawValue, parentViewController: self)
                    }
                })
            } else if let error = error {
                DisplayUtil.displayOkAlert(title: "Error", message: error.localizedDescription, parentViewController: self)
            } else {
                DisplayUtil.displayOkAlert(title: "Error", message: ErrorUtil.ErrorMessage.genericError.rawValue, parentViewController: self)
            }
        }
    }


    @IBAction func didTapCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        switch textField {
        case emailAddressTextField:
            passwordTextField.becomeFirstResponder()
        case passwordTextField:
            didTapLogin(self)
        default:
            break
        }
        return true
    }
}
