//
//  LoanApplicationFormViewController.swift
//  MoneyMeTask
//
//  Created by Richmond Ko on 08/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import UIKit

class LoanApplicationFormViewController: UIViewController {

    // MARK: - Stored
    private var loanAmountPicker = UIPickerView()
    private var loanDurationPicker = UIPickerView()
    private var birthdayDatePicker = UIDatePicker()
    var loanApplicationFormViewModel: LoanApplicationFormViewModel!

    enum PickerViewTag: Int {
        case loanAmountPicker
        case loanDurationPicker
    }

    // MARK: - Stored (IBOutlet)
    @IBOutlet var loanAmountTextField: UITextField!
    @IBOutlet var loanDurationTextField: UITextField!
    @IBOutlet var paymentLabel: UILabel!
    @IBOutlet var loanReasonTextField: UITextField!
    @IBOutlet var titleTextField: UITextField!
    @IBOutlet var firstNameTextfield: UITextField!
    @IBOutlet var lastNameTextField: UITextField!
    @IBOutlet var dateOfBirthTextfield: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var mobileNumberTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var submitActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var submitButton: DesignableButton!
    
    // MARK: - App View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewModel()
        configureUI()
        hideActivityIndicator()
    }

    // MARK: - Instance
    static func instantiateFromStoryboard() -> LoanApplicationFormViewController {
        return UIStoryboard(name: "LoanApplicationForm", bundle: nil).instantiateViewController(withIdentifier: "LoanApplicationFormViewController") as! LoanApplicationFormViewController
    }

    private func configureUI() {
        configureLoanAmountPicker()
        configureLoanDurationPicker()
        configureBirthdayPicker()
        loanAmountTextField.inputView = loanAmountPicker
        loanDurationTextField.inputView = loanDurationPicker
        dateOfBirthTextfield.inputView = birthdayDatePicker
        loanAmountTextField.text = loanApplicationFormViewModel.loanAmountString
        loanDurationTextField.text = loanApplicationFormViewModel.loanDurationString

        titleTextField.text = loanApplicationFormViewModel.title
        firstNameTextfield.text = loanApplicationFormViewModel.firstName
        lastNameTextField.text = loanApplicationFormViewModel.lastName
        mobileNumberTextField.text = loanApplicationFormViewModel.mobileNumber
        emailTextField.text = loanApplicationFormViewModel.emailAddress

        if let _ = loanApplicationFormViewModel.loggedInUser {
            emailTextField.isHidden = true
            passwordTextField.isHidden = true
        }

        loanReasonTextField.delegate = self
        titleTextField.delegate = self
        firstNameTextfield.delegate = self
        lastNameTextField.delegate = self
        emailTextField.delegate = self
        mobileNumberTextField.delegate = self
    }

    private func configureViewModel() {
        loanApplicationFormViewModel.initialize()

        loanApplicationFormViewModel.loanAmount.bind { [weak self] (newValue) in
            guard let self = self else { return }
            self.loanApplicationFormViewModel.configureLoanAmount()
            self.loanApplicationFormViewModel.configurePMT()
            self.loanAmountTextField.text = self.loanApplicationFormViewModel.loanAmountString
            self.paymentLabel.text = self.loanApplicationFormViewModel.loanPMT
        }

        loanApplicationFormViewModel.loanDuration.bind { [weak self] (newValue) in
            guard let self = self else { return }
            self.loanApplicationFormViewModel.configureLoanDuration()
            self.loanApplicationFormViewModel.configurePMT()
            self.loanDurationTextField.text = self.loanApplicationFormViewModel.loanDurationString
            self.paymentLabel.text = self.loanApplicationFormViewModel.loanPMT
        }

        loanApplicationFormViewModel.selectedBirthday.bind { [weak self] (birthday) in
            guard let self = self else { return }
            self.dateOfBirthTextfield.text = birthday?.toString()
        }
    }

    private func configureLoanAmountPicker() {
        loanAmountPicker.dataSource = self
        loanAmountPicker.delegate = self
        loanAmountPicker.tag = PickerViewTag.loanAmountPicker.rawValue
    }

    private func configureLoanDurationPicker() {
        loanDurationPicker.dataSource = self
        loanDurationPicker.delegate = self
        loanDurationPicker.tag = PickerViewTag.loanDurationPicker.rawValue
    }

    private func configureBirthdayPicker() {
        birthdayDatePicker.addTarget(self, action: #selector(datePickerDidChange(_:)), for: .valueChanged)
        birthdayDatePicker.maximumDate = Date()
        birthdayDatePicker.date = Date()
        birthdayDatePicker.datePickerMode = .date
    }

    @objc private func datePickerDidChange(_ sender: UIDatePicker) {
        loanApplicationFormViewModel.selectedBirthday.value = sender.date
    }

    private func submitUserInfo() {
        getFormDataAndBindToViewModel()

        if loanApplicationFormViewModel.isFormValidated() {
            if let _ = loanApplicationFormViewModel.loggedInUser {
                submitNewLoan()
            } else {
                signupAndSubmitNewLoan()
            }
        }
    }

    private func getFormDataAndBindToViewModel() {
        loanApplicationFormViewModel.loanReason = loanReasonTextField.text ?? ""
        loanApplicationFormViewModel.firstName = firstNameTextfield.text ?? ""
        loanApplicationFormViewModel.lastName = lastNameTextField.text ?? ""
        loanApplicationFormViewModel.mobileNumber = mobileNumberTextField.text ?? ""
        loanApplicationFormViewModel.emailAddress = emailTextField.text ?? ""
        loanApplicationFormViewModel.password = passwordTextField.text ?? ""
        loanApplicationFormViewModel.title = titleTextField.text ?? ""
    }

    private func signupAndSubmitNewLoan() {
        showActivityIndicator()
        FirebaseService.shared.signup(loanApplicationFormViewModel: loanApplicationFormViewModel) { [weak self] (success, error) in
            guard let self = self else { return }
            self.hideActivityIndicator()
            self.handleSubmitLoanResponse(success: success, error: error)
        }
    }

    private func submitNewLoan() {
        showActivityIndicator()
        FirebaseService.shared.createLoanEntry(loanApplicationFormViewModel: loanApplicationFormViewModel, userId: loanApplicationFormViewModel.loggedInUser!.id!) { [weak self] (success, error) in
            guard let self = self else { return }
            self.hideActivityIndicator()
            self.handleSubmitLoanResponse(success: success, error: error)
        }
    }

    private func handleSubmitLoanResponse(success: Bool, error: Error?) {
        if success {
            DisplayUtil.displayOkAlert(message: "Loan Application Sent!", parentViewController: self, completionHandler: {
                self.dismiss(animated: true, completion: nil)
            })
        } else if let error = error {
            DisplayUtil.displayOkAlert(title: "Error", message: error.localizedDescription, parentViewController: self)
        } else {
            DisplayUtil.displayOkAlert(title: "Error", message: ErrorUtil.ErrorMessage.genericError.rawValue, parentViewController: self)
        }
    }

    private func showActivityIndicator() {
        submitActivityIndicator.startAnimating()
        submitActivityIndicator.isHidden = false
        submitButton.isEnabled = false
    }

    private func hideActivityIndicator() {
        submitActivityIndicator.stopAnimating()
        submitActivityIndicator.isHidden = true
        submitButton.isEnabled = true
    }

    // MARK: - Instance (IBAction)
    @IBAction func didTapCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func didTapContinueButton(_ sender: Any) {
        submitUserInfo()
    }

}

extension LoanApplicationFormViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let pickerViewTag = PickerViewTag(rawValue: pickerView.tag)!

        switch pickerViewTag {
        case .loanAmountPicker:
            return loanApplicationFormViewModel.loanIncrements.count
        case .loanDurationPicker:
            return loanApplicationFormViewModel.loanDurationIncrements.count
        }
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let pickerViewTag = PickerViewTag(rawValue: pickerView.tag)!

        switch pickerViewTag {
        case .loanAmountPicker:
            return "$\(loanApplicationFormViewModel.loanIncrements[row].formatWithDecimal)"
        case .loanDurationPicker:
            return "\(loanApplicationFormViewModel.loanDurationIncrements[row].formatWithDecimal) months"
        }
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let pickerViewTag = PickerViewTag(rawValue: pickerView.tag)!

        switch pickerViewTag {
        case .loanAmountPicker:
            loanApplicationFormViewModel.loanAmount.value = loanApplicationFormViewModel.loanIncrements[row]
        case .loanDurationPicker:
            loanApplicationFormViewModel.loanDuration.value = loanApplicationFormViewModel.loanDurationIncrements[row]
        }

    }
}

extension LoanApplicationFormViewController: LoanApplicationFormViewModelDelegate {
    func showValidationError(withMessage message: String) {
        DisplayUtil.displayOkAlert(text: message, parentViewController: self)
    }
}

extension LoanApplicationFormViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        switch textField {
        case loanReasonTextField:
            titleTextField.becomeFirstResponder()
        case titleTextField:
            firstNameTextfield.becomeFirstResponder()
        case firstNameTextfield:
            lastNameTextField.becomeFirstResponder()
        case lastNameTextField:
            dateOfBirthTextfield.becomeFirstResponder()
        case dateOfBirthTextfield:
            emailTextField.becomeFirstResponder()
        case emailTextField:
            mobileNumberTextField.becomeFirstResponder()
        case mobileNumberTextField:
            passwordTextField.becomeFirstResponder()
        case passwordTextField:
            break
        default:
            break
        }
        return true
    }
}
