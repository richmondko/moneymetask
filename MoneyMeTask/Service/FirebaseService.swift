//
//  FirebaseService.swift
//  MoneyMeTask
//
//  Created by Richmond Ko on 08/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import Foundation
import Firebase
import CodableFirebase

class FirebaseService {
    private init() {
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
    }
    private let db = Firestore.firestore()
    static let shared = FirebaseService()

    private func createNewUser(email: String, password: String, completion: @escaping (User?, Error?) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            if let user = result?.user {
                completion(user, nil)
            } else if let error = error {
                completion(nil, error)
            } else {
                completion(nil, nil)
            }
        }
    }

    private func createUserEntry(loanApplicationFormViewModel: LoanApplicationFormViewModel, user: User, completion: @escaping (Bool, Error?) -> Void) {
        let reference = db.collection("users")
        reference.document("\(user.uid)").setData( [
            MMUser.CodingKeys.title.rawValue: loanApplicationFormViewModel.title,
            MMUser.CodingKeys.firstName.rawValue: loanApplicationFormViewModel.firstName,
            MMUser.CodingKeys.lastName.rawValue: loanApplicationFormViewModel.lastName,
            MMUser.CodingKeys.dateOfBirth.rawValue: loanApplicationFormViewModel.selectedBirthday.value?.timeIntervalSince1970 ?? "",
            MMUser.CodingKeys.email.rawValue: loanApplicationFormViewModel.emailAddress,
            MMUser.CodingKeys.mobileNumber.rawValue: loanApplicationFormViewModel.mobileNumber
        ]) { (error) in
            if let error = error {
                completion(false, error)
            } else {
                completion(true, nil)
            }
        }
    }

    func signup(loanApplicationFormViewModel: LoanApplicationFormViewModel, completion: @escaping (Bool, Error?) -> Void) {
        createNewUser(email: loanApplicationFormViewModel.emailAddress, password: loanApplicationFormViewModel.password) { [weak self] (user, error) in
            if let user = user {
                self?.createUserEntry(loanApplicationFormViewModel: loanApplicationFormViewModel, user: user, completion: { (success, error) in
                    if success {
                        self?.createLoanEntry(loanApplicationFormViewModel: loanApplicationFormViewModel, userId: user.uid, completion: { (success, error) in
                            completion(success, error)
                        })
                    } else {
                        completion(false, nil)
                    }
                })
            } else if let error = error {
                completion(false, error)
            } else {
                completion(false, nil)
            }
        }
    }

    func createLoanEntry(loanApplicationFormViewModel: LoanApplicationFormViewModel, userId: String, completion: @escaping (Bool, Error?) -> Void) {
        let reference = db.collection("loans")
        reference.addDocument(data: [
            MMLoan.CodingKeys.userId.rawValue: userId,
            MMLoan.CodingKeys.loanAmount.rawValue: loanApplicationFormViewModel.loanAmount.value,
            MMLoan.CodingKeys.loanDuration.rawValue: loanApplicationFormViewModel.loanDuration.value,
            MMLoan.CodingKeys.loanReason.rawValue: loanApplicationFormViewModel.loanReason
        ]) { (error) in
            if let error = error {
                completion(false, error)
            } else {
                completion(true, nil)
            }
        }
    }

    func loginUser(email: String, password: String, completion: @escaping(User?, Error?) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            completion(result?.user, error)
        }
    }

    func getUserEntry(userId: String, completion: @escaping (MMUser?, Error?) -> Void) {
        let reference = db.collection("users")
        reference.document("\(userId)").getDocument { (document, error) in
            if let document = document, document.exists {
                var mmUser = try! FirestoreDecoder().decode(MMUser.self, from: document.data()!)
                mmUser.id = userId
                completion(mmUser, nil)
            } else {
                completion(nil, error)
            }
        }
    }
}
